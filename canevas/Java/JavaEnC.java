public class JavaEnC {
	public native void AllPairs( String s1 , String s2 ) ;
	public native void BentleyOttman( String s1 , String s2 ) ;
	
	public static void main ( String [] args ) {
		new JavaEnC().AllPairs( "infilename" , "outfilename" ) ;
		new JavaEnC().BentleyOttman( "infilename" , "outfilename" ) ;
		
	}
	
	static {
		System.loadLibrary("exemple") ;
	}		
}
