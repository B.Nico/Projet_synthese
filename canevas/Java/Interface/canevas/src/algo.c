#include <stdlib.h>
#include <stdio.h>
#include "algo.h"
#include "tree.h"
#include "geometry.h"

/*
 * ranger dans une liste les segments stockés
 * dans le fichier texte de nom infilename
 * dont la première ligne est un entier indiquant
 * le nombre de segments et les lignes suivantes
 * les coordonnées des segments.
 * chaque ligne est composée de 2 couples.
 * Le premier couple est les coordonnées du début du segment
 * au format : xnum / xden ; pour l'abscisse
 * et : ynum / yden ; pour l'ordonnée.
 * Le second couple est la fin du segment
 */
List * load_segments(char *infilename) {
  int i;
  List *segments = (List*) calloc(1, sizeof(List));

  FILE *fptr;
  if ((fptr = fopen(infilename,"r")) == NULL) {
    printf("Error while opening file %s.", infilename);
    exit(1);
  }
  int size;
  fscanf(fptr, "%d", &size);

  for (i = 0; i < size; i++) {
    long a1, b1, c1, d1, a2, b2, c2, d2;
    fscanf(fptr, "%ld/%ld,%ld/%ld", &a1, &b1, &c1, &d1);
    fscanf(fptr, "%ld/%ld,%ld/%ld", &a2, &b2, &c2, &d2);
    Segment *s = (Segment*) malloc(sizeof(Segment));
    Point p1 = {{a1,b1},{c1,d1}};
    Point p2 = {{a2,b2},{c2,d2}};
    if (point_prec(p1, p2)) {
      (*s).begin = p1;
      (*s).end = p2;
    }
    else {
      (*s).begin = p2;
      (*s).end = p1;
    }
    list_append(segments, s);
  }
  fclose(fptr);
  return segments;
}

/*
 * ranger dans un fichier texte de nom outfilename
 * les points de la liste *intersections.
 * La premier ligne indique le nombre de points.
 * Puis chaque ligne contient les coordonnée de chaque point.
 * Le format des coordonnées est le même que pour les segments.
 */
void save_intersections(char *outfilename, List *intersections) {
  int i;
  FILE *fptr;
  if ((fptr = fopen(outfilename,"w")) == NULL) {
    printf("Error while opening file %s.\n", outfilename);
    exit(1);
  }

  fprintf(fptr, "%d\n", intersections->size);
  LNode *curr = intersections->head;
  for (i = 0; i < intersections->size; i++) {
    fprintf(fptr, "%ld/%ld,%ld/%ld\n",
            ((Point*) curr->data)->x.num,
            ((Point*) curr->data)->x.den,
            ((Point*) curr->data)->y.num,
            ((Point*) curr->data)->y.den);
    curr = curr->next;
  }
}

/*
 * exécute l'algorithme glouton sur les segments rangés
 * dans le fichier texte de nom infilename et range les points
 * d'intersection dans le fichier texte de nom outfilename
 */
void allPairs(char *infilename, char *outfilename) {

	List *Segments = (List *)malloc(sizeof(List)); //Création de la Liste
	Segments = load_segments(infilename); //Copie du fichier dans la liste
	List *ResInter = (List*)calloc(1,sizeof(List)); //Liste des intersections
	LNode *Temp = (LNode*)calloc(1,sizeof(LNode)); //Node provisoire, qui pointe toujours sur la tête de Liste
	LNode *Test = (LNode*)calloc(1,sizeof(LNode)); //Node provisoire, qui est le Node qui parcours le reste de la Liste pour le Test
	Segment *T1 = (Segment*)calloc(1,sizeof(Segment)); //Segment de Test
	Segment *T2 = (Segment*)calloc(1,sizeof(Segment)); //Segment de Test
	//Variables locales
	int NbSeg, i, j;
	
	NbSeg = Segments->size; //Fixe le Nombre d'éléments dans la Liste Segments
	Temp = (Segment*)Segments->head; //Fixe la tête de la Liste Segments
	
	for(i = 0; i < NbSeg - 1; i++) //Boucle qui permet de Parcourrir toute la Liste Segments, stop lorsque la Liste est Vide
	{
		T1 = (Segment*)Temp->data; //Fixe le Premier Segment, correspondant à la Tete de la Liste
		Test = Temp->next; //Fixe le Premier node de Test (qui est le second de la liste Segments)
		for( j = 0; j < NbSeg - i - 1; j++) //Boucle qui permet de Tester tout les ellements de la Liste Segments avec le Premier Node fixé , s'arrete lorsque le reste de la liste est Vide
		{
			T2 = (Segment*)Test->data; //Fixe le Second Segment pour le Test
			if(intersect(*T1,*T2) == 1) { //S'il existe une intersection, alors on la calcule et on la stock dans la liste prévue : ResInter
				list_append(ResInter,getIntersectionPoint(*T1,*T2));
			}
			Test = Test->next; //Pointe Test sur l'élément suivant pour la suite du Traitement
		}
		list_remove_first(Segments); //Enlève la tête de Segments pour la suite du traitement
		Temp = (Segment*)Segments->head; //Pointe Temp sur la Nouvelle tête de Segments
	}
	list_destroy(Segments); //Vide le(s) dernier(s) éléments de la Liste
	save_intersections(outfilename,ResInter); //Sauvegarde la liste ResInter dans un fichier (ici outfilename)
}

/*
 * exécute l'algorithme Bentley-Ottmmann sur les segments rangés
 * dans le fichier texte de nom infilename et range les points
 * d'intersection dans le fichier texte de nom outfilename
 */
void BentleyOttmmann(char *infilename, char *outfilename) {
	List *Segments; //Création de la Liste
	Segments = load_segments(infilename); //Copie du fichier dans la liste
	
	List *ResInter = (List*)calloc(1,sizeof(List)); //Liste des intersections
	List *SegActif = (List*)calloc(1,sizeof(List)); //Liste des segments Atifs
	//Création de l'arbre des évènements
	EventTree * A;
	EventNode * Node;
	Point key;
	int deb = 1; //Cette variable sert a reconnaitre si le point est le début ou la fin d'un segment
				 //Si deb = 1, c'est le début du segment, si deb = 02, c'est la fin du segment
	//Initialisation du premier point  //(Point) (*(Segment*)Segments->head->data).begin
	key.x=(*(Segment*)Segments->head->data).begin.x;
	key.y=(*(Segment*)Segments->head->data).begin.y;
		
	//Initialisation de l'arbre
	Node = new_event(key,deb,Segments->head->data,NULL);
	A->root = Node;
	A->size = 1;
	//Mise a jour de la liste.
	list_remove_first(Segments);
	
	//Création de l'arbre des evenements
	while(Segments->size != 0)
	{
		if(deb == 1) { 
			deb = 2;
		}else if(deb == 2){
			deb = 1;
		}
		key.x=(*(Segment*)Segments->head->data).begin.x;
		key.y=(*(Segment*)Segments->head->data).begin.y;
		Node = new_event(key,deb,Segments->head->data,NULL);
		insert_event(A,Node);
		list_remove_first(Segments);
	}
	
	//Taitement
	while(A->size != 0){
		Node = get_next_event(A);
		if(Node->type = 0){
			GestionIntersectionEvent(A,Node,SegActif,ResInter);
		}
		if(Node->type = 1){
			
		}
		if(Node->type = 2){
			
		}
	}
	
	//Fin du programme
	list_destroy(Segments); //Vide le(s) dernier(s) éléments de la Liste
	save_intersections(outfilename,ResInter); //Sauvegarde la liste ResInter dans un fichier (ici outfilename)
}
