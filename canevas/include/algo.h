#ifndef ALGO_H_
#define ALGO_H_

#include "list.h"
#include "tree.h"

/*
 * ranger dans une liste les segments stockés
 * dans le fichier texte de nom infilename
 * dont la première ligne est un entier indiquant
 * le nombre de segments et les lignes suivantes
 * les coordonnées des segments.
 * chaque ligne est composée de 2 couples.
 * Le premier couple est les coordonnées du début du segment
 * au format : xnum / xden ; pour l'abscisse
 * et : ynum / yden ; pour l'ordonnée.
 * Le second couple est la fin du segment
 */
List * load_segments(char *infilename);

/*
 * ranger dans un fichier texte de nom outfilename
 * les points de la liste *intersections.
 * La premier ligne indique le nombre de points.
 * Puis chaque ligne contient les coordonnée de chaque point.
 * Le format des coordonnées est le même que pour les segments.
 */
void save_intersections(char *outfilename, List *intersections);

/*
 * exécute l'algorithme glouton sur les segments rangés
 * dans le fichier texte de nom infilename et range les points
 * d'intersection dans le fichier texte de nom outfilename
 */
void allPairs(char *infilename, char *outfilename);

/*
* Cette fonction sert a cast un node en Segment.
*/
Segment * nodeToSeg(LNode * N);

/*
* Cette fonction sert a Traiter dans l'arbre'des evenement, un Node qui est le Debut d'un segment.
*/
void Traiter_Debut_Segment(EventTree * listePrio, EventNode * NEvent, List * SegActif);

/*
* Cette fonction sert a Traiter dans l'arbre'des evenement, un Node qui est la Fin d'un segment.
*/
void Traiter_Fin_Segment(EventTree * listePrio, EventNode * NEvent, List * SegActif);

/*
*renvoie 1 si le point P est dans la liste inters
*/
int list_posess_point(List * inters, Point p);

/*
* Cette fonction sert a Traiter dans l'arbre'des evenement, un Node qui est une Intersection,
* et l'ajoute dans la Liste ResInter.
*/
void Traiter_Inter_Segment(EventTree * listePrio, EventNode * NEvent, List * SegActif, List * ResInter);

/*
 * exécute l'algorithme Bentley-Ottmmann sur les segments rangés
 * dans le fichier texte de nom infilename et range les points
 * d'intersection dans le fichier texte de nom outfilename
 */
void BentleyOttmmann(char *infilename, char *outfilename);
#endif
