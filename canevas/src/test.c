#include <stdlib.h>
#include <stdio.h>
#include "geometry.h"
#include "list.h"
#include "tree.h"
#include "algo.h"

int main() {

  // Segment d’ origine (1 ,1) et d’ extrémité (6 ,1)
  //{ { {1 ,1} , {1 ,1} } , { {6 ,1} , {1 ,1} } }
  // Segment d’ origine (1 ,1) et d’ extrémité (6 ,6)
  //{ { {1 ,1} , {1 ,1} } , { {6 ,1} , {6 ,1} } }

  allPairs((char*) "./data/input", (char*) "./data/output");
  BentleyOttmmann((char*) "./data/input", (char*) "./data/output2");
  return EXIT_SUCCESS;
}
