#include "JavaEnC.h"
#include "util.h"

JNIEXPORT void JNICALL Java_MyClass_printStrings(JNIEnv * env , jobject obj , jstring s1 , jstring s2) 
{
      const char * string1 = (*env) -> GetStringUTFChars (env , s1 , NULL ) ;
      const char * string2 = (*env) -> GetStringUTFChars (env , s2 , NULL ) ;
      myprint ( string1 ) ;
      myprint ( string2 ) ;
}