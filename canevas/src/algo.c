#include <stdlib.h>
#include <stdio.h>
#include "algo.h"
#include "tree.h"
#include "geometry.h"

/*
 * ranger dans une liste les segments stockés
 * dans le fichier texte de nom infilename
 * dont la première ligne est un entier indiquant
 * le nombre de segments et les lignes suivantes
 * les coordonnées des segments.
 * chaque ligne est composée de 2 couples.
 * Le premier couple est les coordonnées du début du segment
 * au format : xnum / xden ; pour l'abscisse
 * et : ynum / yden ; pour l'ordonnée.
 * Le second couple est la fin du segment
 */
List * load_segments(char *infilename) {
  int i;
  List *segments = (List*) calloc(1, sizeof(List));

  FILE *fptr;
  if ((fptr = fopen(infilename,"r")) == NULL) {
    printf("Error while opening file %s.", infilename);
    exit(1);
  }
  int size;
  fscanf(fptr, "%d", &size);

  for (i = 0; i < size; i++) {
    long a1, b1, c1, d1, a2, b2, c2, d2;
    fscanf(fptr, "%ld/%ld,%ld/%ld", &a1, &b1, &c1, &d1);
    fscanf(fptr, "%ld/%ld,%ld/%ld", &a2, &b2, &c2, &d2);
    Segment *s = (Segment*) malloc(sizeof(Segment));
    Point p1 = {{a1,b1},{c1,d1}};
    Point p2 = {{a2,b2},{c2,d2}};
    if (point_prec(p1, p2)) {
      (*s).begin = p1;
      (*s).end = p2;
    }
    else {
      (*s).begin = p2;
      (*s).end = p1;
    }
    list_append(segments, s);
  }
  fclose(fptr);
  return segments;
}

/*
 * ranger dans un fichier texte de nom outfilename
 * les points de la liste *intersections.
 * La premier ligne indique le nombre de points.
 * Puis chaque ligne contient les coordonnée de chaque point.
 * Le format des coordonnées est le même que pour les segments.
 */
void save_intersections(char *outfilename, List *intersections) {
  int i;
  FILE *fptr;
  if ((fptr = fopen(outfilename,"w")) == NULL) {
    printf("Error while opening file %s.\n", outfilename);
    exit(1);
  }

  fprintf(fptr, "%d\n", intersections->size);
  LNode *curr = intersections->head;
  for (i = 0; i < intersections->size; i++) {
    fprintf(fptr, "%ld/%ld,%ld/%ld\n",
            ((Point*) curr->data)->x.num,
            ((Point*) curr->data)->x.den,
            ((Point*) curr->data)->y.num,
            ((Point*) curr->data)->y.den);
    curr = curr->next;
  }
  fclose(fptr);
}

/*
 * exécute l'algorithme glouton sur les segments rangés
 * dans le fichier texte de nom infilename et range les points
 * d'intersection dans le fichier texte de nom outfilename
 */
void allPairs(char *infilename, char *outfilename) {

	List *Segments = (List *)malloc(sizeof(List)); //Création de la Liste
	Segments = load_segments(infilename); //Copie du fichier dans la liste
	List *ResInter = (List*)calloc(1,sizeof(List)); //Liste des intersections
	LNode *Temp = (LNode*)calloc(1,sizeof(LNode)); //Node provisoire, qui pointe toujours sur la tête de Liste
	LNode *Test = (LNode*)calloc(1,sizeof(LNode)); //Node provisoire, qui est le Node qui parcours le reste de la Liste pour le Test
	Segment *T1 = (Segment*)calloc(1,sizeof(Segment)); //Segment de Test
	Segment *T2 = (Segment*)calloc(1,sizeof(Segment)); //Segment de Test
	//Variables locales
	int NbSeg, i, j;
	
	NbSeg = Segments->size; //Fixe le Nombre d'éléments dans la Liste Segments
	Temp = (Segment*)Segments->head; //Fixe la tête de la Liste Segments
	
	for(i = 0; i < NbSeg - 1; i++) //Boucle qui permet de Parcourrir toute la Liste Segments, stop lorsque la Liste est Vide
	{
		T1 = (Segment*)Temp->data; //Fixe le Premier Segment, correspondant à la Tete de la Liste
		Test = Temp->next; //Fixe le Premier node de Test (qui est le second de la liste Segments)
		for( j = 0; j < NbSeg - i - 1; j++) //Boucle qui permet de Tester tout les ellements de la Liste Segments avec le Premier Node fixé , s'arrete lorsque le reste de la liste est Vide
		{
			T2 = (Segment*)Test->data; //Fixe le Second Segment pour le Test
			if(intersect(*T1,*T2) == 1) { //S'il existe une intersection, alors on la calcule et on la stock dans la liste prévue : ResInter
				list_append(ResInter,getIntersectionPoint(*T1,*T2));
			}
			Test = Test->next; //Pointe Test sur l'élément suivant pour la suite du Traitement
		}
		list_remove_first(Segments); //Enlève la tête de Segments pour la suite du traitement
		Temp = (Segment*)Segments->head; //Pointe Temp sur la Nouvelle tête de Segments
	}
	list_destroy(Segments); //Vide le(s) dernier(s) éléments de la Liste
	save_intersections(outfilename,ResInter); //Sauvegarde la liste ResInter dans un fichier (ici outfilename)
}

/*
* Cette fonction sert a cast un node en Segment.
*/
Segment * nodeToSeg(LNode * Node){
	Segment * S =  (Segment *)Node->data;
	return S;
}

/*
* Cette fonction sert a Traiter dans l'arbre'des evenement, un Node qui est le Debut d'un segment.
*/
void Traiter_Debut_Segment(EventTree * A, EventNode * event, List * SegActif){
	if(SegActif->size == 0){
		list_append(SegActif,event->s1); // si notre liste de seg actif est vide, on ajoute ce segment 
	}
	else{
		LNode * Node = SegActif->head;
		Segment * seg = event->s1;
		Segment * segFromList = nodeToSeg(Node); 
		Rational x = event->key.x;
		
		int continu = 1;
		
		if(seg_prec(*seg,*segFromList,x)){
		//si notre segment est avant le premier segment de la liste, on le rajoute au debut et on le compare au second
			list_prepend(SegActif,seg);
			if(intersect(*seg,*segFromList)){
				if(!(event_exists(A,*getIntersectionPoint(*seg,*segFromList)))){
				//si le point d'intersection existe on l'ajoute dans l'arbre A
					insert_event(A,new_event(*getIntersectionPoint(*seg,*segFromList),0,seg,segFromList));
				}
			}
		}
		else
		{
			LNode * NPrec = SegActif->head;
			Node = Node->next;
			while(Node != NULL && continu == 1){
				segFromList = nodeToSeg(Node);
				if(seg_prec(*seg,*segFromList,x)){
					continu = 0;
				}
				else
				{
					NPrec = Node;
					Node = Node->next;
				}
			}

			list_insert_after(SegActif,seg,NPrec);
			
			if(intersect(*seg,*nodeToSeg(NPrec))){
			//on l'a rajouté a sa place et on le compare au seg precedent (qui existe), puis au seg suivant (s'il existe)
				Point * pInter = (Point *)malloc(sizeof(Point));
				pInter = getIntersectionPoint(*seg,*nodeToSeg(NPrec));
				if(!(event_exists(A,*pInter))){
					insert_event(A,new_event(*pInter,0,seg,nodeToSeg(NPrec)));
				}
			}
			if(Node != NULL){
				if(intersect(*seg,*nodeToSeg(Node))){
					if(!(event_exists(A,*getIntersectionPoint(*seg,*nodeToSeg(Node))))){
						insert_event(A,new_event(*getIntersectionPoint(*seg,*nodeToSeg(Node)),0,seg,nodeToSeg(Node)));
					}
				}
			}
		}
	}
}

/*
* Cette fonction sert a Traiter dans l'arbre'des evenement, un Node qui est la Fin d'un segment.
*/
void Traiter_Fin_Segment(EventTree * A, EventNode * NEvent, List * SegActif){
	
	LNode * Node = SegActif->head;
	LNode * NBefore = (LNode *)malloc(sizeof(LNode));
	NBefore = NULL;

	Point End = NEvent->key;
	Point Temp;
	int continu = 1;

	while(Node != NULL && continu == 1){
		Temp = nodeToSeg(Node)->end;
		if(eq(Temp.x,End.x) && eq(Temp.y,End.y)){
			continu = 0;
		}
		else
		{
			NBefore = Node;
			Node = Node->next;
		}
	}
	if(Node != NULL && NBefore != NULL && Node->next != NULL){
		if(intersect(*nodeToSeg(NBefore),*nodeToSeg(Node->next))){
			if(!(event_exists(A,*getIntersectionPoint(*nodeToSeg(NBefore),*nodeToSeg(Node->next))))){
				insert_event(A,new_event(*getIntersectionPoint(*nodeToSeg(NBefore),*nodeToSeg(Node->next)),0,nodeToSeg(NBefore),nodeToSeg(Node->next)));
			}
		}
	}
	
	if(Node != NULL){ //on l'a bien trouvé
		list_remove_node(SegActif,Node);
	}
}

/*
*renvoie 1 si le point P est dans la liste inters
*/
int list_posess_point(List * ResInter, Point p){
	Point Temp;
	LNode * Node = ResInter->head;
	
	while(Node != NULL){
		Temp = *((Point *)Node->data);
		if(eq(Temp.x,p.x) && eq(Temp.y,p.y)){
			return(1);
		}
		Node = Node->next;
	}
	return 0;
}

/*
* Cette fonction sert a Traiter dans l'arbre'des evenement, un Node qui est une Intersection,
* et l'ajoute dans la Liste ResInter.
*/
void Traiter_Inter_Segment(EventTree * A, EventNode * event, List * SegActif, List * ResInter){
	Point * inter = &event->key;
	list_append(ResInter,inter);
	Segment S1 = *event->s1;
	Segment S2 = *event->s2;

	Point DebS1 = S1.begin;
	Point DebS2 = S2.begin;

	Point Temp;
	int continu = 1;

	LNode * Node = SegActif->head;

	while(continu && Node != NULL){
		Temp = nodeToSeg(Node)->begin;
		if((eq(Temp.x,DebS1.x) && eq(Temp.y,DebS1.y)) || (eq(Temp.x,DebS2.x) && eq(Temp.y,DebS2.y))){ 
			continu = 0;
		}
		else
		{
			Node = Node->next;
		}
	}
	
	if(Node->prev != NULL){
		if(intersect(*nodeToSeg(Node->prev),*nodeToSeg(Node->next))){
			if(!(event_exists(A,*getIntersectionPoint(*nodeToSeg(Node->prev),*nodeToSeg(Node->next)))) && !(list_posess_point(ResInter,*getIntersectionPoint(*nodeToSeg(Node->prev),*nodeToSeg(Node->next))))){
				insert_event(A,new_event(*getIntersectionPoint(*nodeToSeg(Node->prev),*nodeToSeg(Node->next)),0,nodeToSeg(Node->prev),nodeToSeg(Node->next)));
			}
		}
	}
	
	if(Node->next->next != NULL){
		if(intersect(*nodeToSeg(Node),*nodeToSeg(Node->next->next))){
			if(!(event_exists(A,*getIntersectionPoint(*nodeToSeg(Node),*nodeToSeg(Node->next->next)))) && !(list_posess_point(ResInter,*getIntersectionPoint(*nodeToSeg(Node),*nodeToSeg(Node->next->next))))){
				insert_event(A,new_event(*getIntersectionPoint(*nodeToSeg(Node),*nodeToSeg(Node->next->next)),0,nodeToSeg(Node),nodeToSeg(Node->next->next)));
			}
		}
	}
	
	if(Node != NULL){
	//on echange les segments de place dans la liste vu qu'ils se sont croisé, puis on vérifie si les nouveaux voisins ont un point d'intersection
		list_exchange_curr_next(SegActif,Node);
	}
}

/*
 * exécute l'algorithme Bentley-Ottmmann sur les segments rangés
 * dans le fichier texte de nom infilename et range les points
 * d'intersection dans le fichier texte de nom outfilename
 */
void BentleyOttmmann(char *infilename, char *outfilename) {

	List *Segments; //Création de la Liste
	Segments = load_segments(infilename); //Copie du fichier dans la liste

	List *ResInter = (List*)malloc(sizeof(List)); //Liste des intersections
	ResInter->head = NULL;
	ResInter->tail = NULL;
	ResInter->size = 0;

	List *SegActif = (List*)malloc(sizeof(List)); //Liste des segments Atifs
	SegActif->head = NULL;
	SegActif->tail = NULL;
	SegActif->size = 0;

	//Création de l'arbre des évènements
	EventTree * A = (EventTree*)malloc(sizeof(EventTree));
	A->size = 0;
	A->root = NULL;

	//Création du node de Traitement
	EventNode * Node = (EventNode *)malloc(sizeof(EventNode));
	Point key; //Point qui sert a l'ajout dans l'arbre des evenements
	
	//Création de l'arbre des evenements
	while(Segments->size != 0)
	{
		//Ajout du point de Debut du segment
		key.x=(*(Segment*)Segments->head->data).begin.x;
		key.y=(*(Segment*)Segments->head->data).begin.y;
		Node = new_event(key,1,Segments->head->data,NULL);
		insert_event(A,Node);
		//Ajout du point de Fin du segment
		key.x=(*(Segment*)Segments->head->data).end.x;
		key.y=(*(Segment*)Segments->head->data).end.y;
		Node = new_event(key,2,Segments->head->data,NULL);
		insert_event(A,Node);
		//Mise a jour de la Liste
		list_remove_first(Segments);
	}

	//Taitement
	while(A->size != 0){
		Node = get_next_event(A);
		if(Node->type == 0){ // On rentre ICI si le NODE actif est du type Intersection
			Traiter_Inter_Segment(A,Node,SegActif,ResInter);
		}
		if(Node->type == 1){ // On rentre ICI si le NODE actif est du type BEGIN
			Traiter_Debut_Segment(A,Node,SegActif);
		}
		if(Node->type == 2){ // On rentre ICI si le NODE actif est du type END
			Traiter_Fin_Segment(A,Node,SegActif);
		}
	}
	
	//Fin du programme
	list_destroy(Segments); //Vide le(s) dernier(s) éléments de la Liste
	save_intersections(outfilename,ResInter); //Sauvegarde la liste ResInter dans un fichier (ici outfilename)
}