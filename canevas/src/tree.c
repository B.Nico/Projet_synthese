#include <stdlib.h>
#include <stdio.h>
#include "tree.h"

static void preorder(EventNode *node) {
  if (node != NULL) {
    display_point(node->key);
    printf("\n");
    preorder(node->left);
    preorder(node->right);
  }
}

static void inorder(EventNode *node) {
  if (node != NULL) {
    inorder(node->left);
    display_point(node->key);
    printf("\n");
    inorder(node->right);
  }
}

static void postorder(EventNode *node) {
  if (node != NULL) {
    postorder(node->left);
    postorder(node->right);
    display_point(node->key);
    printf("\n");
  }
}

// order = 0 (preorder), 1 (inorder), 2 (postprder)
void display_tree_keys(const EventTree *tree, int order) {
  switch (order) {
    case 0:
      preorder(tree->root);
      break;
    case 1:
      inorder(tree->root);
      break;
    case 2:
      postorder(tree->root);
      break;
    default:
      printf("display_tree_keys: non valid order parameter\n");
      exit(1);
  }
}

/*
 * renvoie un nouveau EventNode d'attribut
 *  <key, type, s1, s2, NULL, NULL>
 */
EventNode * new_event(Point key, int type, Segment *s1, Segment *s2) {
  EventNode *eventNode=(EventNode*)malloc(sizeof(EventNode));
  if(eventNode != NULL){
	  eventNode->key = key;
	  eventNode->type = type;
	  eventNode->s1 = s1;
	  eventNode->s2 = s2;
	  eventNode->left =NULL;
	  eventNode->right=NULL;
  }
  return eventNode;
}

/*
 * recherche la place dans tree du nouvel événement event
 * et l'insère.
 * L'ABR est modifié et la modification peut porter sur sa racine.
 */
void insert_event(EventTree *tree, EventNode *event) {
	if (tree->size == 0){
		tree->root = event;
	}
	else
	{
		int continu = 1;
		EventNode * NCurr = tree->root;
		while(continu)
		{
			if (point_prec(event->key, NCurr->key)){ 
				// Si event key precede noeud key, alors on l'insère à gauche
				if(NCurr->left != NULL){
					NCurr = NCurr->left;
				}
				else
				{
					continu = 0;
				}
			}
			else
			{
				if (point_prec(NCurr->key, event->key)){ 
					// Si noeud key precede event key, alors on l'insère à droite
					if(NCurr->right != NULL){
						NCurr = NCurr->right;
					}
					else
					{
						continu = 0;
					}				
				}
			}
		}
		if(point_prec(event->key, NCurr->key)){
			NCurr->left = event;
		}
		else
		{
			NCurr->right = event;
		}
	}
	tree->size++;
}

/*
 * trouve le prochain événement(plus grande priorité),le supprime de l'arbre et le renvoie
 */
EventNode* get_next_event(EventTree *tree) {
	int continu = 1;
	EventNode * NCurr = tree->root;
	EventNode * noeudpere = tree->root;
	while(NCurr->left != NULL){
		noeudpere = NCurr;
		NCurr = NCurr->left;
	}
	if(NCurr != tree->root){
		noeudpere->left = NCurr->right;
	}else{
		tree->root = tree->root->right;
	}
	tree->size=tree->size - 1;
	return(NCurr);
}

/*
 * renvoie 1 si le clef key existe dans l'ABR tree, O sinon
 */
int event_exists(EventTree *tree, Point key) {
  int boucle = 1;
  EventNode * temp=tree->root;
  while(boucle==1){
	  if(point_prec(key, temp->key)){
		  if(temp->left != NULL){
			  temp = temp->left;
		  }
		  else{
			  boucle=0;
		  }
	  }
	  else{
		  if(point_prec(temp->key, key)){
			  if(temp->right != NULL){
				  temp = temp->right;
			  }
			  else{
				  boucle=0;
			  }
		  }
		  else{
			  return 1;
		  }
	  }
  }
  return 0;
}
