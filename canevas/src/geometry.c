#include <stdio.h>
#include <stdlib.h>
#include "geometry.h"

/*
 * affiche le point p
 */
void display_point(const Point p) {
  printf("(");
  display_rational(p.x);
  printf(",");
  display_rational(p.y);
  printf(")");
}

/*
 * affiche le segment s
 */
void display_segment(const Segment s) {
  printf("from: ");
  display_point(s.begin);
  printf(" to: ");
  display_point(s.end);
}

/*
 * renvoie 1 si le point d'intersection key1 précède le point
 * d'intersection key2, 0 sinon
 */
int point_prec(Point key1, Point key2) {
  //s2 précède s1, s2.yx > s1.x
  if( gt(key2.x,key1.x)==1){	
		return 0;
  }
  else
	{
		return 1;
  } 
}

/*
 * renvoie 1 si s1 précède s2, 0 sinon
 */
int seg_prec(Segment s1, Segment s2, Rational x) {
  Rational m1, m2;
	Rational p1, p2;
	
	//On calcul l'équation de la droite S1 : y1 = m1*x1 + p1
	m1 = rdiv(rsub(s1.end.y,s1.begin.y),rsub(s1.end.x,s1.begin.x)); // On calcul m1
	p1 = rsub(s1.begin.y,rmul(m1,s1.begin.x)); // On calcul p1

	//On calcul l'équation de la droite S2 : y2 = m2*x2 + p2
	m2 = rdiv(rsub(s2.end.y,s2.begin.y),rsub(s2.end.x,s2.begin.x)); // On calcul m2
	p2 = rsub(s2.begin.y,rmul(m2,s2.begin.x)); //On calcul p2
	
	Point * x1 = (Point *)malloc(sizeof(Point));
	Point * x2 = (Point *)malloc(sizeof(Point));
	
	x1->x = x;
	x1->y = radd(rmul(m1,x),p1);

	x2->x = x;
	x2->y = radd(rmul(m2,x),p2);
	
	//retourne 1 si x1 précède x2, sinon 0, si c'est x2 qui précède x1
	return (point_prec(*x1,*x2)); 
}

/*
 * renvoie 1 si s1 et s2 ont une intersection, 0 sinon
 */

int intersect(Segment s1, Segment s2) {
	Rational coefS1, coefS2, p1,p2;
	Point intersection;
	//calcul du coef directeur de S1(y2-y1)/(x2-x1)
	coefS1 = rdiv(rsub(s1.end.y,s1.begin.y),rsub(s1.end.x,s1.begin.x));
	//calcul du coef directeur de S2(y4-y3)/(x4-x3)
	coefS2 = rdiv(rsub(s2.end.y,s2.begin.y),rsub(s2.end.x,s2.begin.x));

	if (eq(coefS1,coefS2)==0) //droites non parallèles, intersection existe
	{
		//(y1-coefS1*x1)
		p1 = rsub(s1.begin.y,rmul(coefS1,s1.begin.x));
		//(y3-coefS2*x3)
		p2 = rsub(s2.begin.y,rmul(coefS2,s2.begin.x));
		
		//abscisse=(p2-p1)/(coefS1-coefS2)
		intersection.x = rdiv(rsub(p2,p1),rsub(coefS1,coefS2));
		//ordonnée=(coefS1*(xi+p1))
		intersection.y = rmul(coefS1,radd(intersection.x,p1));
		
		//abscisse intersection appartient à s1
		if (( gt(intersection.x,min(s1.begin.x,s1.end.x))) && (lt(intersection.x,max(s1.begin.x,s1.end.x))))
		{
			//abscisse intersection appartient à s2
			if (( gt(intersection.x,min(s2.begin.x,s2.end.x))) && (lt(intersection.x,max(s2.begin.x,s2.end.x)))){
				return 1;
			}
		}
	}
	//droites parallèles, intersection n'existe pas
	return 0;
}    

/*
 * calcule et renvoie le point d'intersection entre s1 et s2
 * on suppose que ce point existe
 */
Point* getIntersectionPoint(Segment s1, Segment s2) {
	Point *intersection = (Point*)calloc(1,sizeof(Point));
	Rational m1, m2;
	Rational p1, p2;
	
	//On calcul l'équation de la droite S1 : y1 = m1*x1 + p1
	m1 = rdiv(rsub(s1.end.y,s1.begin.y),rsub(s1.end.x,s1.begin.x)); // On calcul m1
	p1 = rsub(s1.begin.y,rmul(m1,s1.begin.x)); // On calcul p1

	//On calcul l'équation de la droite S2 : y2 = m2*x2 + p2
	m2 = rdiv(rsub(s2.end.y,s2.begin.y),rsub(s2.end.x,s2.begin.x)); // On calcul m2
	p2 = rsub(s2.begin.y,rmul(m2,s2.begin.x)); //On calcul p2

	//Les droites sont sécantes en un point J dont les coordonnées sont xJ = (p1 – p2) / (m1 – m2) et yJ = (m1 × xJ) + p1.
	intersection->x = rdiv(rsub(p2,p1),rsub(m1,m2));
	intersection->y = radd(rmul(m1,intersection->x),p1);
	
	return intersection;

}
