#include <stdlib.h>
#include <stdio.h>
#include "list.h"

/*
 * créer et renvoie un nouveau nœud (LNode)
 */
static LNode * new_node(void *data) {
  LNode *noeud =(LNode*)malloc(sizeof(LNode));
  if (noeud != NULL){
	  noeud->data=data;
	  noeud->next=NULL;
	  noeud->prev=NULL;
  }
  return noeud;
}

/*
 * créer un élément de liste (LNode)
 * y ranger la donnée (data)
 * insérer l'élément en tête de la liste *list
 */
void list_prepend(List *list, void *data) {
	if(list != NULL){ //Verifie si la liste est non vide
		LNode * New_Ele = new_node(data);
		if(list->head != NULL){ //vérifie qu'il existe déjà une tête de liste
		//head existe
			list->head->prev = New_Ele;
			New_Ele->next = list->head;
			list->head = New_Ele;
		}
		else //head n'existe pas
		{
			list->head = New_Ele;
			list->tail = New_Ele;
		}
		list->size++;
	}
	else //Si la liste est Vide, on renvoie un message d'erreur.
	{
		printf("La liste est VIDE, et ne peut donc pas être consulté/modifié\nfonction list_prepend\n");
	}
}

/*
 * créer un élément de liste (LNode)
 * y ranger la donnée (data)
 * insérer l'élément en queue de la liste *list
 */
void list_append(List *list, void *data) {
	LNode * noeud = new_node(data);
	if(list->tail == NULL){	//liste vide
		list->head=noeud;
		list->tail=noeud;
	}
	else
	{
		list->tail->next=noeud;
		noeud->prev=list->tail;
		list->tail=noeud;
	}
	list->size++;

}

/*
 * créer un élément de liste (LNode)
 * y ranger la donnée (data)
 * insérer l'élément dans la liste *list après l'élément prev
 * ce dernier est supposé appartenir effectivement à la liste
 */
void list_insert_after(List *list, void *data, LNode *curr) {
	if(list != NULL) //Verifie si la liste n'est pas vide
	{
		LNode *noeud=new_node(data);
		if(list->tail == NULL)
		//Si le Node curr est le dernier element, alors le nouveau element seras en queue, donc on appelle la fonction list_append pour insérer en queue de liste.
		{ 
			list_prepend(list,data);
		}
		else
		{
			noeud->prev = curr;
			noeud->next = curr->next;
			curr->next = noeud;
			curr->next->prev = noeud;
		}
		list->size++;		
	}
	else //Si la liste est Vide, on renvoie un message d'erreur.
	{
		printf("La liste est VIDE, et ne peut donc pas être consulté/modifié\nfonction list_insert_after\n");
	}
}


/*
 * supprimer le premier élément de la liste *list
 * si celle-ci n'est pas vide
 */
void list_remove_first(List *list) {
	if(list != NULL) //Verifie si la liste n'est pas vide
	{
		if(list->head == list->tail){
			list->head = NULL;
			list->tail = NULL;
			list->size = 0;
		}
		else
		{
			//mise en place d'une variable temp pour garder en mémoire la tête de la list, pour pouvoir la supprimer.
			List *Temp = (List*)calloc(1,sizeof(List));
			Temp->head = list->head;
			//on pointe la tête de liste sur son second élèment.
			if( list->head->next != NULL){
				list->head = list->head->next;
				list->head->prev = NULL;
			}
			//On supprime l'ancienne tête
			free(Temp->head);
			//On diminue la taille de la liste
			list->size--;
		}
	}
	else //Si la liste est Vide, on renvoie un message d'erreur.
	{
		printf("La liste est VIDE, et ne peut donc pas être supprimer\nfonction list_remove_first\n");
	}
}

/*
 * supprimer le dernier élément de la liste *list
 * si celle-ci n'est pas vide
 */
void list_remove_last(List *list) {
	
	if(list != NULL) //Verifie si la liste n'est pas vide
	{
		if(list->head == list->tail){
			list->head = NULL;
			list->tail = NULL;
			list->size = 0;
		}
		else
		{
			//mise en place d'une variable temp pour garder en mémoire la queue de la liste, pour pouvoir la supprimer.
			List *Temp = (List*)calloc(1,sizeof(List));
			Temp->tail = list->tail;
			//on pointe la tête de liste sur l'avant dernier élement
			if( list->head->prev != NULL){
				list->tail = list->tail->prev;
				list->tail->next = NULL;
			}
			//On supprime l'ancienne queue
			free(Temp->tail);
			//On diminue la taille de la liste
			list->size--;
		}
	}
	else //Si la liste est Vide, on renvoie un message d'erreur.
	{
		printf("La liste est VIDE, et ne peut donc pas être supprimer\nfonction list_remove_last\n");
	}
}


/*
 * supprimer l'élément pointé par node de la liste *list
 * l'élément est supposé appartenir effectivement à la liste
 */
void list_remove_node(List *list, LNode *node) {
	if(list != NULL) //Verifie si la liste n'est pas vide
	{
		
		if(node == list->head){
			list_remove_first(list);
		}
		else if (node == list->tail){
			list_remove_last(list);
		}
		else {
			//mise en place d'une variable temp pour garder en mémoire le node voulus de la liste, pour pouvoir la supprimer.
			LNode *Temp= node;
			//on pointe l'élément avant le node sur l'élément après le node, et inversement.
			if(node->prev!=NULL)
			node->prev->next=node->next;
			if(node->next!=NULL)
			node->next->prev=node->prev;
			//On supprime le noeud en trop.
			free(Temp);
			//On diminue la taille de la liste
			list->size--;
		}
	}
	else //Si la liste est Vide, on renvoie un message d'erreur.
	{
		printf("La liste est VIDE, et ne peut donc pas être supprimer\nfonction list_remove_node\n");
	}
}

/*
 * permute les positions des nœuds curr et curr->next
 * dans la liste list
 */
void list_exchange_curr_next(List *list, LNode *curr) {
	if (curr->next != NULL) {
		void *temp;
		temp = curr->next->data;
		curr->next->data = curr->data;
		curr->data = temp;
	}
	else
	{
		printf("Fin de la LISTE\nlist_exchange_curr_next\n");
	}
}

/*
 * supprimer tous les éléments de la liste *list
 * sans pour autant supprimer leurs données (data)
 * qui sont des pointeurs
 */
void list_destroy(List *list) {
	while(list->size != 0)
	{
		list_remove_first(list);
	}
}
